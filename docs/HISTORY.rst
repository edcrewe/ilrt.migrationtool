Changelog for ilrt.migrationtool
--------------------------------

    (name of developer listed in brackets)

ilrt.migrationtool - 0.5 UNRELEASED

    - Add purge function to run generic setup utility methods 
      NB requires generic setup version 1.4.1 so TODO add try except handling of versions to code

    [Chris Bailey, ILRT - University of Bristol] 

ilrt.migrationtool - 0.4 Release May 8th 2009

    - Remove the paradigm of the sitemigration factory of objects built from users 
      migration scripts since it is uneccessary and leads to pickling problems
    - Add a traceback to out method and use it for generic setup utility method
    - Add a keyword moving subtool based on Products.PloneKeywordManager
    - Add a utility method to allow for temporarily toggling properties during migration
    - Enable a tab to aid debugging migration scripts by running migration methods individually
 
    PLEASE NOTE: If an existing site is upgraded to version 0.4 the site_migration tool
    should be deleted and the quickinstaller method for it run to add a fresh instance

    [Ed Crewe, ILRT - University of Bristol]

ilrt.migrationtool - 0.3 Release - Feb 15th 2009

    - Fixed the workflow tool's transition lookup to handle 
      workflows that require more than one transition between
      certain states.
    - Added utility to save object attributes to the file system
      so that objects can be reinstantiated without losing data.
    - Added tests for the workflow tool and utility
    - Fixed bug related to migration lookup.
    
    [Ed Crewe, ILRT - University of Bristol]

ilrt.migrationtool - 0.2 First release - Jan 30th 2009

    - Subclassed plone migration tool
    - Added migration objects
    - Added workflow migration tool via browser views
    
    [Ed Crewe, ILRT - University of Bristol]

ilrt.migrationtool - 0.1 Unreleased

    - Initial package structure.
    
    [zopeskel]

