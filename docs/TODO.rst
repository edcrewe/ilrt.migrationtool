To Do List
----------

1. Fix the utility for running generic setup steps, since if steps are 
   specified and at the same time this command is called more than once
   generic setup has an id clash on writing its log report.
   Currently the workaround is to not specify the steps when running from
   multiple profiles.

2. Add tests for the migration tool itself.

3. Add a means to pass kwargs on the single migration methods debug tab.
   Enable 'dry run', ie. transaction rollback for single methods.

4. Add other utilities or sub-tools?
