Please download the released eggs and tarballs from [[http://pypi.python.org/pypi/ilrt.migrationtool|pypi]]

See the documentation packaged in them

* [[http://bitbucket.org/edcrewe/ilrt.migrationtool/src/tip/README.rst|README]]
* [[http://bitbucket.org/edcrewe/ilrt.migrationtool/src/tip/docs/HISTORY.rst|HISTORY]]
* [[http://bitbucket.org/edcrewe/ilrt.migrationtool/src/tip/docs/TODO.rst|TODO]]

With additional documentation in the tests

* [[http://bitbucket.org/edcrewe/ilrt.migrationtool/src/tip/ilrt/migrationtool/tests/migrationtool.txt|parent migration tool]]
* [[http://bitbucket.org/edcrewe/ilrt.migrationtool/src/tip/ilrt/migrationtool/tests/browsertool.txt|browser tool]]
* [[http://bitbucket.org/edcrewe/ilrt.migrationtool/src/tip/ilrt/migrationtool/tests/atfitool.txt|AT field migration tool]]
* [[http://bitbucket.org/edcrewe/ilrt.migrationtool/src/tip/ilrt/migrationtool/tests/utils.txt|general utilities]]
* [[http://bitbucket.org/edcrewe/ilrt.migrationtool/src/tip/ilrt/migrationtool/tests/workflow.txt|workflow migration tool]]

